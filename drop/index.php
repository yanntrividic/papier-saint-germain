<!doctype html>
<html lang="en">
<head>
  <title>Drop</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="robots" content="noindex, nofollow">
  <meta name="googlebot" content="noindex, nofollow">
  <link rel="shortcut icon" href="favicon.png"/>
  <style>
    :root{--c:MediumSpringGreen;}
    body{background:#222; color:var(--c); font-family:monospace; font-size:16px;min-height:100vh; margin:0;}
    main{padding:10px;}
    body.drag-enter{background:navy;}
    a{color:inherit; text-decoration:none;}
    .path-line{width:90%; padding:10px; font-size:1.5em; background:#ffffff33; border:0; color:white;}
  </style>
</head>
<body>
  <main>
    Glisser-déposer un fichier ici pour l’uploader sur le serveur
    <br>
    <input type="text" class="path-line">
  </main>
  <script>
    let b = document.body
    // Drag image, upload
    b.addEventListener('dragover', handleDragOver);
    b.addEventListener('dragenter', handleDragEnter);
    b.addEventListener('drop', handleDrop);

    function handleDragOver(event) {event.preventDefault(); b.classList.add('drag-enter')}
    function handleDragEnter(event) {event.preventDefault();}
    function handleDrop(event) {
      event.preventDefault();
      b.classList.remove('drag-enter')
      const file = event.dataTransfer.files[0];
      const name = file.name
      console.log(file.type);
      if (file.type.startsWith('image/') || file.type.startsWith('font')) {
        const reader = new FileReader();
        reader.onload = function(e) {
          imagePath = e.target.result
          uploadFile(imagePath,name)
        };
        reader.readAsDataURL(file);
      } else {
        alert('Please drop a file.');
      }
    }
    function uploadFile(fileData,name) {
      let xhr = new XMLHttpRequest()
      xhr.open('POST', 'save-file.php', true)
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
          console.log(xhr.responseText)
        }else{
          let scriptElement = document.createElement('script');
          scriptElement.innerHTML = xhr.responseText;
          document.body.appendChild(scriptElement);
        }
      };
      xhr.send('name='+ name + '&fileData=' + encodeURIComponent(fileData))
    }

  </script>
</body>
</html>