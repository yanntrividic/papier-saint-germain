<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Fonts</title>
  <style>

body{
  font-size:16px;
  font-family: monospace;
}
section{
  margin-bottom:2em;
  border:1px solid #ccc;
}
section .preview{
  margin:0;
  padding:5px;
  font-size:5em;
}
section .meta{
  background:#eee;
  padding:5px;
  color:#888;
}
.meta code{
  display: block;
  color:black;
  background:#f4f4f4;
}
  </style>
</head>
<body>
<?php
  $woff2Files = glob("*.woff2");
  foreach ($woff2Files as $file) {
    $fontName = explode(".",$file)[0];
    echo "<section>";
    echo "<style>@font-face{font-family: '$fontName'; src: url('$file') format('woff2'); }</style>";
    echo "<p class='preview' contenteditable style='font-family:$fontName;'>$fontName</p>";
    echo "<div class='meta'><p>Déclaration de la fonte: <code>@font-face {font-family: '$fontName'; src: url('fonts/$file'); }</code></p>";
    echo "<p>Appel de la fonte: <code>font-family:$fontName;</code></p></div>";
    echo "</section>";
  }
?>
</ul>
  
</body>
</html>